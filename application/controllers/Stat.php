<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Stat extends BaseController{

	public function __construct()
    {
        parent::__construct();
        //$this->load->model('user_model');
         //$this->load->model('Main_Model');
        $this->isLoggedIn();   
    }
    
	public function index()
	{
		$arr=array('1','2','3','4','5','6','7');
		$this->load->model('stat_model');
		$num=[];
		foreach($arr as $k=>$v)
		{


			$num=$this->stat_model->get_roles_stat($v);
			$type=$this->stat_model->get_role_text($v);
			if($num[0]['num']=='')
			{
				$my[]=array('type'=>$type[0]['role'],'num'=>0);


			}
			else{
				$my[]=array('type'=>$type[0]['role'],'num'=>$num[0]['num']);

			}
			$datas=$this->stat_model->get_roles_data();


		}
		$data['roles']=$my;
		$data['datas']=$datas;
		$total=$this->stat_model->get_roles_total();
		$total=$total->num_rows();
		$data['total']=$total;

		
		
		//$this->loadViews("load_roles_data", $this->global);

		//$this->load->view('load_roles_data',$data);
$this->global['pageTitle'] = 'ARTT : Stat';
        
        $this->loadViews("load_roles_data", $this->global, $data, NULL);

	}
	public function load_data_sp_type(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('type','','required');
		if($this->form_validation->run())
		{

		$type=$this->input->post('type');
		$this->load->model('stat_model');
		$result1=$this->stat_model->get_data_by_sp_type($type);
		//print_r($result);
		$result2=$this->stat_model->get_data_by_roleid($result1[0]['roleId']);
		$i=1;
		
		$tbl='';
		foreach($result2 as $k=>$v)
		{
			$tbl.="<tr><td>".$i."</td><td>".$v['name']."</td><td>".$result1[0]['role']."</td><td>".$v['email']."</td><td>".$v['mobile']."</td><td>".$v['createdDtm']."</td></tr>";
			$i++;


		}
		
		echo $tbl;
	}
	else{
		echo "No Data";

	}


//	$this->$this->form_validation->set_rules('type', 'fieldlabel', 'trim|required|min_length[5]|max_length[12]');


	}
	public function load_program_stat(){
		
		$this->load->model('stat_model');
		$result=$this->stat_model->get_program();
		$data['program']=$result;
		$this->global['pageTitle'] = 'ARTT : Program Charts';
        
        $this->loadViews("programs_stat_view", $this->global, $data, NULL);
		//$this->load->view('programs_stat_view',$data);




	}

public function load_course_stat1()
{
	$this->load->model('stat_model');
	$result=$this->stat_model->get_program();
		$data['program']=$result;
		$this->global['pageTitle'] = 'ARTT : Batch Charts';
        $this->loadViews("load_form_data1", $this->global, $data, NULL);
		

	// $this->load->model('stat_model');
	// $result=$this->stat_model->get_program();
	// $data['result']=$result;

	// $this->load->view('load_form_data',$result);

}
public function get_courses_data1()
{
	$this->load->library('form_validation');
		$this->form_validation->set_rules('program', '', 'required');
		$this->form_validation->set_rules('batch', '', 'required');
		if($this->form_validation->run())
		{
	

	
	$this->load->model('stat_model');
	$courses=$this->stat_model->get_course_data2($this->input->post('program'),$this->input->post('batch'));
	//print_r($courses);
	$tmyarray=array();
	$rmyarray=array();
	$b1myarray=array();
	$b2myarray=array();
// 	$rmyarray=array();
	if(!empty($courses))
 	{
 		foreach($courses as $k=>$v)
 		{
 			// $tmyarray[]=array("name"=>$v['coursename']);
 			// $rmyarray[]=array("TotRcvFeeAmt"=>$v['TotRcvFeeAmt']);
 			// $b1myarray[]=array("b1"=>$v['Bal_1']);
 			// $b2myarray[]=array("b2"=>$v['Bal_2']);
 			// $dmyarray[]=array("discount"=>$v['Discount_Amt']);
 			$tmyarray[]=array("label"=>$v['coursecode']
 ,"y"=>$v['Total_CourseFee']);
 			$rmyarray[]=array("label"=>$v['coursecode']
 ,"y"=>$v['TotRcvFeeAmt']);
 			$b1myarray[]=array("label"=>$v['coursecode']
 ,"y"=>$v['Frz_Rcv_Amt']);
 			$b2myarray[]=array("label"=>$v['coursecode']
 ,"y"=>$v['Bal_2']);
 			$dmyarray[]=array("label"=>$v['coursecode']
 ,"y"=>$v['Discount_Amt']);
// 			$smyarray[]=array("label"=>$v['coursecode']
// ,"y"=>$v['No_Of_Stu']);
			}
			}
	else{
		$tmyarray[]=array("label"=>"No Data"
 ,"y"=>1);
		$rmyarray[]=array("label"=>"No Data"
 ,"y"=>1);
			$b1myarray[]=array("label"=>"No Data" ,"y"=>1);
			$b2myarray[]=array("label"=>"No Data"
 ,"y"=>1);
			$dmyarray[]=array("label"=>"No Data"
 ,"y"=>1);

	}
		
// 		$c=count($tmyarray);
// 		//print_r($tmyarray);
// 		$tcourse=array();
// 		for($i=0;$i<$c;$i++)
// 		{
// 			if($i=0)
// 			{
			 


// 			$tcourse[]=array('label'=>$tmyarray[$i]['name'],"y"=>$rmyarray[$i]['TotRcvFeeAmt']);
// 		}
// 		else{



// 		}

// 		}
		



  		$tmyarray=json_encode($tmyarray, JSON_NUMERIC_CHECK);
  		$rmyarray=json_encode($rmyarray, JSON_NUMERIC_CHECK);
  		$b1myarray=json_encode($b1myarray, JSON_NUMERIC_CHECK);
  		$b2myarray=json_encode($b2myarray, JSON_NUMERIC_CHECK);
  		$dmyarray=json_encode($dmyarray, JSON_NUMERIC_CHECK);
// // 		$rmyarray=json_encode($rmyarray, JSON_NUMERIC_CHECK);
// // 		$smyarray=json_encode($smyarray, JSON_NUMERIC_CHECK);
// // //print_r($smyarray);



  		$data['tmyarray']=$tmyarray;
  		$data['rmyarray']=$rmyarray;
  		$data['b1myarray']=$b1myarray;
  		$data['b2myarray']=$b2myarray;
 		$data['dmyarray']=$dmyarray;
 		$data['courses']=$courses;

		$this->global['pageTitle'] = 'ARTT : Batch Charts';
         $this->loadViews("course_chart_data1", $this->global, $data, NULL);
		

		//$this->load->view('course_chart_data',$data);





}
else{
	//echo validation_errors();
	redirect(base_url().'stat/load_course_stat1');
}

	//print_r($tmyarray);
	

}

	
	public function load_data_by_program(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required');
		if($this->form_validation->run())
		{
		$id=$this->input->post('id');
		$this->load->model('stat_model');
		$data=$this->stat_model->get_data_by_program($id);
		$myarray=[];
		if(count($data)>0)
{
		foreach($data as $k=>$v)
		{
			$count=$this->stat_model->get_count($v['module_id']);
			$myarray[]=array("y"=>$count[0]['count']
,"name"=>$v['module_name'],"id"=>$v['module_id']);


		}
		 echo json_encode($myarray, JSON_NUMERIC_CHECK); 
		
}
else{
	$myarray[]=array("y"=>1,"name"=>"No Data","id"=>"");
	echo json_encode($myarray, JSON_NUMERIC_CHECK);
}		

}
else{
	echo "No data";

}



	}
	public function load_data_by_module(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id','','required');
		if($this->form_validation->run())
		{
		$id=$this->input->post('id');
		$this->load->model('stat_model');
		$module=$this->stat_model->get_data_by_module1($id);
		$table='';

$i=1;
if(count($module)>0){
		foreach($module as $k=>$v)
		{
			$table.='<tr>';
			$table.='<td>'.$i.'</td><td>'.$v['coursename'].'</td><td>'.$v['coursecode'].'</td><td>'.$v['coursefee'].'</td></tr>';
			$i++;

        }
        echo $table;



	}
	else{
	echo "";

}
}
else{
	echo "";

}
}
public function load_batch_stat(){
		
		$this->load->model('stat_model');
		$result=$this->stat_model->get_program();
		$data['program']=$result;
		$this->global['pageTitle'] = 'ARTT : Batch Charts';
        $this->loadViews("batch_view", $this->global, $data, NULL);
		
	}
	public function load_data_by_program_batch()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required');
		if($this->form_validation->run())
		{
		$id=$this->input->post('id');
		$this->load->model('stat_model');
		$data=$this->stat_model->get_data_by_program_batch($id);
		$myarray=[];

		if(count($data)>0)
 {
		foreach($data as $k=>$v)
		{
			
			$myarray[]=array("y"=>$v['CourseFee']
,"name"=>$v['batch_name']." Total Fee","batchid"=>$v['batch_id'],"namef"=>
$v['batch_name'],"rec"=>$v['Rcv_Amt']);
				$myarray[]=array("y"=>$v['Rcv_Amt']
,"name"=>$v['batch_name']." Received Fees","batchid"=>$v['batch_id']);

		}
		 echo json_encode($myarray, JSON_NUMERIC_CHECK); 
		
}
else{
	$myarray[]=array("y"=>1,"name"=>"No Data","id"=>"");
	echo json_encode($myarray, JSON_NUMERIC_CHECK);
}		

}
else{
	echo "No data";

}



	}
	public function load_courses_total(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required');
		
		if($this->form_validation->run())
		{
			$id=$this->input->post('id');
			$this->load->model('stat_model');
			$batch=$this->stat_model->getData_batch($id);
			$myarray[]=array("y"=>$batch[0]['CourseFee']
,"name"=>$batch[0]['batch_name']);
			$myarray[]=array("y"=>$batch[0]['Rcv_Amt']
,"name"=>$batch[0]['batch_name']);
			
echo json_encode($myarray, JSON_NUMERIC_CHECK);
		
			//print_r($batch);
			
			




		}


	}
	public  function load_data_by_batch_total(){
			$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required');
		if($this->form_validation->run())
		{
		$id=$this->input->post('id');
		$this->load->model('stat_model');
		//$batcht=$this->stat_model->get_batch_total($id);
		// $batch[0]['received'];
		$batch=$this->stat_model->get_batch_course($id);
		$myarray=array();
		foreach($batch as $k=>$v)
		{
			$course=$this->stat_model->get_course_name($v['course_id']);
			$myarray[]=array("y"=>1,"name"=>$course[0]['coursename'],"batch_id"=>$id,"coursefee"=>$course[0]['coursefee'],"courseid"=>$course[0]['course_id']);

		}
		echo json_encode($myarray, JSON_NUMERIC_CHECK);








	}



	}
	public function load_course_total(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('batchid', '', 'required');
		$this->form_validation->set_rules('courseid', '', 'required');
		if($this->form_validation->run())
		{
		$this->load->model('stat_model');
			$courseid=$this->input->post('courseid');
			$batchid=$this->input->post('batchid');
			$fee=$this->stat_model->get_course_fee($courseid);
			$nostu=$this->stat_model->get_student_number($courseid,$batchid);
			$total=$fee[0]['coursefee']*$nostu[0]['student'];
			$paidfee=$this->stat_model->get_paid_amount($courseid,$batchid);
			$myarray[]=array('y'=>$total,'name'=>"Total Fee");
			$myarray[]=array('y'=>$paidfee[0]['received'],'name'=>"Total Fee Paid");
			echo json_encode($myarray, JSON_NUMERIC_CHECK);


			//print_r($fee[0]['coursefee']." ".$nostu[0]['student']." ".$total." ".$paidfee[0]['received']);

	}

}
public function load_data_courses(){
	$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required');
		if($this->form_validation->run())
		{
		$id=$this->input->post('id');
			$this->load->model('stat_model');
			$batch=$this->stat_model->getData_batch_detail($id);
		$table='<tr><thead><th>Batch</th><th>Name of Student</th><th>Course Name</th><th>Actual Amount</th><th>Received Amount</th><th>Remaining Amount</th></tr>';
		foreach($batch as $k=>$v)
		{
			
			$r=$v['coursefee']-$v['received_amount'];
			$table.='<tr><td>'.$v['batch_name'].'</td>
			<td>'.$v['fname'].'</td>
			<td>'.$v['coursename'].'</td>
			<td>'.$v['coursefee'].'</td>
			<td>'.$v['received_amount'].'</td>
			<td>'.$r.'</td>
			



			</tr>

			';



		}
		echo $table;
		//print_r($batch);

		



}

}
public function load_course_stat()
{
	$this->load->model('stat_model');
	$result=$this->stat_model->get_program();
		$data['program']=$result;
		$this->global['pageTitle'] = 'ARTT : Batch Charts';
        $this->loadViews("load_form_data", $this->global, $data, NULL);
		

	// $this->load->model('stat_model');
	// $result=$this->stat_model->get_program();
	// $data['result']=$result;

	// $this->load->view('load_form_data',$result);

}
public function get_batches()
{
	$this->load->model('stat_model');
	$result=$this->stat_model->get_batches1($this->input->post('id'));
	if($result->num_rows())
	{
		$option='';
		$result=$result->result_array();
		foreach($result as $k=>$v)
		{
			$option.="<option value=".$v['batch_id'].">".$v['batch_name']."</option>";



		}
		echo $option;



	}
	



}
public function get_courses_data()
{
	$this->load->model('stat_model');
	$courses=$this->stat_model->get_course_data($this->input->post('program'),$this->input->post('batch'));
	$amyarray=array();
	$rmyarray=array();
	if(!empty($courses))
	{
	
		foreach($courses as $k=>$v)
		{
			$amyarray[]=array("label"=>$v['coursecode']
,"y"=>$v['TotalCourseFees']);
			$rmyarray[]=array("label"=>$v['coursecode']
,"y"=>$v['TotalRCV_AMT']);
			$smyarray[]=array("label"=>$v['coursecode']
,"y"=>$v['No_Of_Stu']);
			

		}

	

		$amyarray=json_encode($amyarray, JSON_NUMERIC_CHECK);
		$rmyarray=json_encode($rmyarray, JSON_NUMERIC_CHECK);
		$smyarray=json_encode($smyarray, JSON_NUMERIC_CHECK);
//print_r($smyarray);



		$data['actual']=$amyarray;
		$data['received']=$rmyarray;
		$data['student']=$smyarray;

		$this->global['pageTitle'] = 'ARTT : Batch Charts';
        $this->loadViews("course_chart_data", $this->global, $data, NULL);
		

		//$this->load->view('course_chart_data',$data);




	}
	else{
		$amyarray[]=array("label"=>'No Data'
,"y"=>'0');
			$rmyarray[]=array("label"=>'No Data'
,"y"=>'0');
			$amyarray=json_encode($amyarray, JSON_NUMERIC_CHECK);
		$rmyarray=json_encode($rmyarray, JSON_NUMERIC_CHECK);


		$data['actual']=$amyarray;
		$data['received']=$rmyarray;

		$this->global['pageTitle'] = 'ARTT : Batch Charts';
        $this->loadViews("course_chart_data", $this->global, $data, NULL);
		
		

		

	}

}





public function student_stats()
{
	$this->load->model('stat_model');
	$students=$this->stat_model->get_students_data();
	$status=$this->stat_model->get_students_data1();
	$sarray=array();
	$starray=array();
	$recarray=array();
	$farray=array();
	$uarray=array();
	$rarray=array();
	$afarray=array();
	$auarray=array();
	$ararray=array();

	if(!empty($students))
	{
		//print_r($students);
		foreach($students as $k=>$v)
		{
		$sarray[]=array("label"=>$v['program_name']
,"y"=>$v['No_Of_Student']);

       }
	}
	else
	{
		$sarray[]=array("label"=>"No Data","y"=>1);

	}

	foreach($status as $k=>$v)
	{
		$farray[]=array('label'=>$v['program_name'],"y"=>$v['Freez_Student']);
		$uarray[]=array('label'=>$v['program_name'],"y"=>$v['UnFreez_Student']);
		$rarray[]=array('label'=>$v['program_name'],"y"=>$v['Reg_Student']);
		$afarray[]=array('label'=>$v['program_name'],"y"=>$v['Freez_Amount']);
		$auarray[]=array('label'=>$v['program_name'],"y"=>$v['UnFreez_Amount']);
		$ararray[]=array('label'=>$v['program_name'],"y"=>$v['Reg_Amount']);
		

	}
	
	
	$data['students']=json_encode($sarray, JSON_NUMERIC_CHECK);
	$data['farray']=json_encode($farray, JSON_NUMERIC_CHECK);
	$data['uarray']=json_encode($uarray, JSON_NUMERIC_CHECK);
	$data['rarray']=json_encode($rarray, JSON_NUMERIC_CHECK);
	$data['afarray']=json_encode($afarray, JSON_NUMERIC_CHECK);
	$data['auarray']=json_encode($auarray, JSON_NUMERIC_CHECK);
	$data['ararray']=json_encode($ararray, JSON_NUMERIC_CHECK);
	
	//$data['recarray']=json_encode($recarray,JSON_NUMERIC_CHECK);
	//print_r($starray);

	$this->global['pageTitle'] = 'ARTT : Students Charts';
       $this->loadViews("student_stat_view", $this->global, $data, NULL);
       //$this->load->view('load_Student_Stats',$data);
		





}
































}