
<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';

class Program_report extends BaseController{
	public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
         $this->load->model('Main_Model');
        $this->isLoggedIn();   
    }
    
	public function index()
	{
		$this->load->model('accordion');
		$result=$this->accordion->get_data();
		if($result->num_rows()>0)
		{
			$data['result']=$result->result_array();

		}
		else{
			$data['result']="No Data";

		}
		//$this->load->view('program_report_view',$data);
		$this->global['pageTitle'] = 'ARTT : Program_drilldown';
		//$data['nodata']="";
        
        $this->loadViews("program_report_view", $this->global, $data, NULL);

	}
	public function load_program_id(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required|numeric');
		if($this->form_validation->run())
		{
		
		$id= $this->input->post('id');
		$this->load->model('accordion');
		$result1=$this->accordion->get_programs_batch($id);
		if($result1->num_rows()>0)
		{
			$i=1;
			$total=0;
			$panel='';
			$result=$result1->result_array();
			foreach($result as $k=>$v)
			{

				$panel.="<div class='panel panel-primary ' ><div  class='panel-heading'><a  id='program".$i."' href='javascript:;'style='color:#fff;'onclick='get_students(".$v['batch_id'].",".$id.",".$i.")'>".$v['batch_name']." Actual Fee :".$v['CourseFee']." || Received Fee : ".$v['Rcv_Amt']."</a></div><div class='panel-body' id='body".$i."' style='display:none'></div></div>";
				$i++;
				$total++;



			}
			$total=$total-1;
			echo $panel.'<div style="display:none" id="total" data-value="'.$total.'"></div>';



		}
	}



	}
	public function load_students(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required|numeric');
		$this->form_validation->set_rules('batch', '', 'required|numeric');
		$this->form_validation->set_rules('program', '', 'required|numeric');
		if($this->form_validation->run())
		{
		
		$id= $this->input->post('id');
		$batch= $this->input->post('batch');
		$program= $this->input->post('program');
		$this->load->model('accordion');
		$result1=$this->accordion->get_students($id,$batch,$program);
		if($result1->num_rows()>0)
		{
			$i=1;
			$panel='<div class="box-body table-responsive"><table class="table table-striped table-bordered" id="example2"><thead><tr><th>S.NO</th><th>Student Name</th><th>Father Name</th>
			<th>Batch Name</th><th>Enrolled Course</th></tr></thead><tbody>';
			$result=$result1->result_array();
			foreach($result as $k=>$v)
			{
				if($v['rec']>0)
				{

				$panel.="<tr id='".$v['batch_id'].$i."'><td><b>".$i."</b></td><td>".$v['fname']."</td><td>".$v['fathername']."</td><td>".$v['batch_name']."</td>
				<td><button class='btn btn-success' onclick='get_course_detail(".$v['program_id'].",
			".$v['batch_id'].",".$v['student_id'].",".$i.")'>+</button></td></tr><tr class='inv' id='courserow".$v['batch_id'].$i."'><td id='course".$v['batch_id'].$i."' colspan='5'></td></tr>";
				$i++;
			}


			}
			echo $panel."</tbody></table></div>";



		}
	}


	}
	public function load_students_course(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('id', '', 'required|numeric');
		$this->form_validation->set_rules('batch', '', 'required|numeric');
		$this->form_validation->set_rules('program', '', 'required|numeric');
		if($this->form_validation->run())
		{

		$id= $this->input->post('id');
		$batch= $this->input->post('batch');
		$program= $this->input->post('program');
		$this->load->model('accordion');
		$result1=$this->accordion->get_students_courses($id,$batch,$program);
		$table='<h4 style="text-align:center;background-color:#036;color:#fff;padding:5px;">Courses Details</h4><table class="table table-striped"><thead><tr><th>S.No</th><th>Course Name</th><th>Course Fee</th><th> Received Amount</th><th> Discount Amount</th></tr></thead><tbody>';
		if($result1->num_rows()>0)
		{
			$i=1;
			$result=$result1->result_array();
			foreach($result as $k=>$v)
			{
				if($v['received_amount']!=0)
				{
				$table.='<tr><td>'.$i.'</td><td>'.$v['coursename'].'</td><td>'.$v['coursefee'].'<td>'.$v['received_amount'].'</td>'.'<td>'.$v['discount_amount'].'</td></tr>';
			         $i++;

		}
		}
			$table.='</tbody></table>';
			echo $table;




		



		}



	}
}

}