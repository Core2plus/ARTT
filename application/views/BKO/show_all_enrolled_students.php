
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">Batch Transfer</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Batch Transfer
                            </h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                              <a href="<?php echo base_url('Main/enroll_std'); ?>">
      <button type="button" class="btn btn-success"><i class="ti-plus"></i></button></a>

                                <h4 class="mt-0 header-title">Batch Transfer</h4>
                                
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Student</th>
                                        <th>Program</th>
                                        <th>Module</th>
                                        <th>Batch</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                       
                                   
                                   <?php
     
     //                   print_r($students);
       //                 die();


        foreach($enroll_student->result() as $enroll_student) {
         
      ?>      
            <tr>   
                            
                <td><?php echo $enroll_student->enrollment_id; ?></td>
                <td><?php echo $enroll_student->fname; ?></td>
                <td><?php echo $enroll_student->program_name; ?></td>
                <td><?php echo $enroll_student->module_name; ?></td>

                <td><a href="<?php echo base_url('Main/show_enrolled_student/').$enroll_student->student_id; ?>"><?php echo $enroll_student->batch_name; ?></a></td>
                                        
                <td>  
        <a href="<?php echo base_url('Main/show_data_to_update_enroll_student/').$enroll_student->enrollment_id; ?>">
        <button type="button" class="btn btn-success "><i class="ti-pencil-alt"></i></button></a>
      
        <a href="<?php echo base_url('Main/delete_enroll_students/').$enroll_student->enrollment_id; ?>">
        <button type="button" class="btn btn-danger"><i class="ti-cut"></i></button></a>
                </td>

            </tr>
                                   <?php   } ?>

                              
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>
