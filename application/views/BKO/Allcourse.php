
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">All Courses</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Courses</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                              <a href="<?php echo base_url('Main/NewCourse'); ?>">
      <button type="button" class="btn btn-success"><i class="ti-plus"></i></button></a>

                                <h4 class="mt-0 header-title">Course</h4>
                                
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Course code</th>
                                        <th>Course name</th>
                                        <th>Course fees</th>
                                        <th>Module Name</th>
                                        <th>Course Duration</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                         <?php 
                                        $no = 1;
                                        foreach($show_courses as $show):
                                        ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $show->coursecode; ?></td>
                                        <td>  <textarea style = "border:none;  " bold rows="2" cols="35" name="usrtxt" wrap="hard">
 <?php echo $show->coursename;  ?>
</textarea></td>
                                        <td><?php echo $show->coursefee; ?></td>
                                          <td><?php echo $show->module_name; ?></td>
                                        <td><?php echo $show->co_duration; ?>
                                            </td>
                                        <td>
    

         <a href="<?php echo base_url('Main/detailed_course/'.$show->course_id); ?>">
      <button type="button" class="btn btn-success"><i class="ti-archive"></i></button></a>
    
         <a href="<?php echo base_url('Main/update_course/'.$show->course_id); ?>">
      <button type="button" class="btn btn-warning"><i class="ti-pencil-alt"></i></button></a>
    
       <a href="<?php echo base_url('Main/delete_course/'.$show->course_id); ?>">
      <button type="button" class="btn btn-danger"><i class="ti-cut"></i></button></a>


                                        </td>
                                   </tr>
                               <?php endforeach; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>