
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                    <li class="breadcrumb-item active">All Teachers</li>
                                </ol>
                            </div>
                            <h4 class="page-title">TEACHERS</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->
    <div class="row" style="margin: 0px;">
        <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['success']))
            {
                ?>
                <div class="alert alert-success">
                    <?php
                        echo $_SESSION['success'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
            <div class="col-sm-12 col-md-6">
        <?php
            if(isset($_SESSION['error']))
            {
                ?>
                <div class="alert alert-danger">
                    <?php
                        echo $_SESSION['error'];
                    ?>
                </div>
                <?php
            }
        ?>
    </div>
</div>

                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                              <a href="<?php echo base_url('Main/teacher'); ?>">
      <button type="button" class="btn btn-success"><i class="ti-plus"></i></button></a>

                                <h4 class="mt-0 header-title">Teachers</h4>
                                
                                <table id="datatable" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Picture</th>
                                        <th>Gender</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                        


                                         <?php
     
     //                   print_r($students);
       //                 die();


        foreach($teachers->result() as $teacher) {
         
        
      ?>
                                    <tr>
                                         <td><?php echo $teacher->teacher_id; ?></td>
                                        <td><?php  echo $teacher->first_name; ?></td>
                                        <td><?php   echo $teacher->email; ?></td>
                                        <td> <img style = "margin-left:5  0px; border-radius: 80%;";  height="50";  width="50"; src="<?php echo base_url(); ?>assets/images/users/<?php echo $teacher->image; ?>" alt=""></td> 
                                        <td><?php   echo $teacher->gender; ?></td>

                                      
                                        <td>
    
<a href="<?php echo base_url('Main/show_enrolled_teachers/').$teacher->teacher_id;?>">
      <button type="button" class="btn btn-primary "><i class="ti-archive"></i></button></a>

        <a href="<?php echo base_url('Main/show_teacher_profile/').$teacher->teacher_id; ?>">
      <button type="button" class="btn btn-success "><i class="ti-user"></i></button></a>

         <a href="<?php echo base_url('Main/update_teacher/').$teacher->teacher_id; ?>">
      <button type="button" class="btn btn-warning "><i class="ti-pencil-alt"></i></button></a>
      
       <a href="<?php echo base_url('Main/delete_student/').$teacher->teacher_id; ?>">
      <button type="button" class="btn btn-danger"><i class="ti-cut"></i></button></a>


                                        </td>
                                   </tr>
                               <?php } ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->

               

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
        <?php $this->load->view('BKO/includes/footer'); ?>
