
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Discount</li>
                                </ol>
                            </div>
                            <h4 class="page-title">In-Voice Generate</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">
                            <?php  foreach($discount_student->result() as $key) {?>
                                <h4 class="mt-0 header-title">Discount </h4>                
                                <form action="<?php echo base_url('Main/insert_discount/').$key->studentid; ?>" method="post">
                                     
                                     <div class="col-md-4">
                                    
                                        <label>ARTT_ID<span class="required">*</span></label>
                                        
                                    
                                            
                                        <input type="text"  disabled value="<?php echo $key->artt_id;?>" class="form-control">

                                    
                                        </div>
                                         
                                     
                                      <div class="col-md-4">
                                   
                                        <label>Student Name<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                        <input type="text" name="fname"  disabled value="<?php echo $key->fname;?>" class="form-control">
                                    
                                        </div>
                                        </div>
                                    <?php } ?>
                                    <div class="col-md-4">
                                      <label>Batch<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                            
                                               
                                    <?php  foreach($discount_batch->result() as $key2) {?>
                                        <input type="hidden" name="batch_id" value="<?php echo $key2->batch_id; ?>">

                                    <input type="text" name="batch_name"  disabled value="<?php echo $key2->batch_name;?>" class="form-control">

                                    <?php
                                        break;}                                    ?>
                                           
                                        </div>
                                    </div>

                                
                                         <div class="col-md-4">
                                      <label>Total Fees<span class="required">*</span></label>
                                        
                                   
                                                                        
                                    <?php $c=0; foreach($discount_batch->result() as $key3) { $c=$c+$key3->coursefee;?>

                                    

                                    <?php
                                        }    
                                         ?>
                                        <input type="text" id="input1"   disabled value="<?php echo $c; ?>" class="form-control">
                                           
                                        </div>
                                         <div class="col-md-4">
                                        <label>Percentage<span class="required">*</span></label>
                                       
                                            <input type="number" id="input2" maxlength='100' max="100" name="percentage"  class="form-control">
                                    
                                        </div>
                                          <div class="col-md-4">
                                        <label>discount amount<span class="required">*</span></label>
                                       
                                            <input type="number" disabled name="output" id="output" placeholder="discount amount " class="form-control">
                                    
                                        </div>
                                    
                                   


                                                                      
                                   
                                    <div class="form-group">
                                        <label>Note</label>
                                        <div>
                                            <textarea class="form-control form-group" name="note" placeholder="Enter Your Reason.."></textarea>
                                        </div>
                                    </div>

                                </div>
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                                Submit
                                            </button>
                                            
                                        </div>
                                    </div>


                                </form>
<a href="<?php echo base_url('Main/modules'); ?>">
                                            <button style="float: right; margin: -52px 0px;" type="reset" class="btn btn-secondary waves-effect m-l-5">
                                                Cancel
                                            </button></a>
                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
     
<script>
$("#input2,#input1").keyup(function () {
    $('#output').val(($('#input1').val() * $('#input2').val()) /100);
});

function calc(){
   var textValue1 = document.getElementById('input1').value;
   var textValue2 = document.getElementById('input2').value;

   document.getElementById('output').value = (textValue1 * textValue2) ; 
 }

</script>