<style>
.disabledTab{
  display: none;
}
</style>
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>
     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">New Student</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Student</h4>
                        </div>
               
                
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                    <div class="offset-lg-1 col-lg-10">
                        <div class="card">
                            <div class="card-body">
                             <?php foreach($std_data->result_array() as $key) {?>
    <form action="<?php echo base_url('Main/edit_student/').$key['studentid']; ?>" method="post" enctype="multipart/form-data">  
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">Student Details</a></li>
    <li><a data-toggle="tab" href="#menu2">Guardian</a></li>
    
     

  </ul>    


                     

 <div class="tab-content form-group ">
    <div id="home" class="tab-pane fade in active">
    
                              
                                   
                                    
 

<br>
                                 <div class="row">
                                            <label class="col-sm-2">ARTT ID</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="artt_id" class="form-control" value="<?php echo $key['artt_id']; ?>"   placeholder="Artt ID">
                                            </div>

                                            <label class="col-xs-1">CR#</label>
                                            <div class="col-sm-5" >
                                                <input type="text" name="cr" class="form-control" value="<?php echo $key['cr']; ?>"   placeholder="CR#">
                                            </div>
                                        </div>
                                        <br>
                                        
                                    <div class="row">
                                        
                                            <label class="col-sm-2">Student Name</label>
                                            <div   class=" col-sm-10">
                                                <input type="text" name="fname" required class="form-control" value="<?php echo $key['fname']; ?>"   placeholder="First Name"/>
                                            </div>
                                          
                                        </div>
                                        <br>
                                       
                                        <div class="row">
                                          

                                            <label class="col-sm-2">CNIC</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="cnic" class="form-control" value="<?php echo $key['CNIC']; ?>"   placeholder="Student CNIC">
                                            </div>
                                            
                                        </div>

                                        <br>
                                        <div class="row">
                                            <label class="col-sm-2">Date of Joining</label>
                                            <div class="col-sm-4">
                                                <input type="date" name="DOJ" class="form-control" value="<?php echo $key['DOJ']; ?>"  >
                                            </div>

                                            <label class="col-sm-2">Email</label>
                                            <div class="col-sm-4">
                                                <input type="email" name="email" class="form-control"   value="<?php echo $key['email']; ?>" placeholder="Student Email">
                                            </div>
                                        </div>
                                    <br>
                                        <div class="row">
                                            <label class="col-sm-2">Gender</label>
                                            <div class="col-sm-4">
                                                <label class="radio-inline">&nbsp;
                                                      <input type="radio" name="optradio" value="Male" <?php  if($key['gender']=="Male"){?> checked <?php } ?> >Male
                                                    </label>&nbsp;&nbsp;
                                                    <label class="radio-inline">&nbsp;
                                                      <input type="radio" name="optradio" value="Female" <?php  if($key['gender']=="Female"){?> checked <?php } ?>>Female
                                                    </label>&nbsp;&nbsp;
    
                                            </div>

                                            <label class="col-sm-2">Number</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="number" value="<?php echo $key['phone']; ?>" class="form-control"   placeholder="Student Phone">
                                            </div>
                                        </div>

                                      <br>
                                        <div class="row">
                                            

                                            <label class="col-sm-2">Permanant Address</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" name= "permanent_address"  rows="2" id="comment" placeholder="Enter Your Domicile Address">
                                               <?php echo $key['permanent_address']; ?></textarea>
                                            </div>

        <div class="row">
                                       &nbsp;&nbsp;&nbsp;&nbsp;     <label class="col-md-4">Profile Picture</label>
                                            <div class="col-md-6">
                                                <input type="file" name="image_student" class="form-control form-group">
                                            </div>

                                        </div>
                                             
                                           
                                        </div>          <br>


                                   

                                
                                </div>



                                 <div id="menu2" class="tab-pane fade">
    
   
                                          <h3>Father Details</h3>
                                          <br> 
                                            <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                            <label class="col-xs-2">Father Name</label>
                                            <div  class="col-sm-10">
                                                <input type="text"  required name="father_name" value="<?php echo $key['fathername']; ?>" class="form-control"   placeholder="Father Name">
                                            </div>
                                            </div>
                                           
                                   
                                  

                                        <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                            <label class="col-sm-2">Father Email</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="father_email" value="<?php echo $key['father_email']; ?>" class="form-control"   placeholder="Father Email">
                                            </div>

                                            <label class="col-sm-2  ">Father CNIC</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="father_cnic" value="<?php echo $key['father_cnic']; ?>" class="form-control"  placeholder="Father CNIC">
                                            </div>
                                        </div>
                                        <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                            <label class="col-sm-2">Fathers Phone</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="father_phone" value="<?php echo $key['father_phone']; ?>" placeholder="Phone" class="form-control"  >
                                            </div>

                                            <label class="col-sm-2">Father Profession</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="father_profession" value="<?php echo $key['father_profession']; ?>" class="form-control"   placeholder="Father's Profession">
                                            </div>

                                        </div>     <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                               
                                           
                                  
                                    </div>
                                    

 <div class="form-group row col-sm-10 col-sm-10 col-xs-12">
                                              <div  class="col-sm-6">
                           <a data-toggle="tab" href="#menu3">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">
                                            </a>
                                                Submit
                                            </button>
                                          </div>
                                        </div>


                                          </div>

 
                                     
   <?php } ?>
  </form>

  </div>



                                </div>
                              

                            </div>
                        </div>
                    </div> <!-- end col -->

                    
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
       
<?php $this->load->view('BKO/includes/footer'); ?>
<script>
    function course_change()
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('GET','ajax_student_courses?batch_id='+document.getElementById('batch_id').value,false);
        xmlhttp.send(null);
        
        document.getElementById('course').innerHTML=xmlhttp.responseText;
    }
</script>
     
