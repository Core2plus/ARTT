
<?php $this->load->view('BKO/includes/header'); ?>
        <!-- Navigation Bar-->
<?php $this->load->view('BKO/includes/aside'); ?>


     
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="offset-sm-1 col-sm-10">
                        <div class="page-title-box">
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">ARTT</a></li>
                                   
                                    <li class="breadcrumb-item active">Course Inserted</li>
                                </ol>
                            </div>
                            <h4 class="page-title">Detail</h4>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->

            
                <div class="row">
                  <div class="offset-lg-1 col-lg-10">
                      <div class="card">
                          <div class="card-body row">
                              <div class="col-md-3">
                                 
                                
                              </div>
                              <div class="col-md-9">
                               
                                  <?php  foreach($detailed_course->result() as $key) { ?>
                                   
                                    <label class="col-md-3">Program Name: </label>
                                    <span><?php echo $key->program_name; ?></span>
                                      <br>
                                    <label class="col-md-3">Module Name: </label>
                                    <span><?php echo $key->module_name; ?></span>
                                      <br>                             
                                    <label class="col-md-3">Course Code: </label>
                                    <span><?php echo $key->coursecode; ?></span>
                                      <br>                             
                                    <label class="col-md-3">Course Name: </label>
                                    <span><?php echo $key->coursename; ?></span>
                                      <br>
                                    <label class="col-md-3">Course Fees: </label>
                                    <span><?php echo $key->coursefee; ?></span>
                                        <br>
                                   <label class="col-md-3">Star Date: </label>
                                    <span><?php echo $key->startdate; ?></span>
                                      <br>                             
                                    
                                    <label class="col-md-3">End Date: </label>
                                    <span><?php echo $key->enddate; ?></span>
                                      <br>                             
                                    <label class="col-md-3">Course Duration: </label>
                                    <span><?php echo $key->co_duration; ?></span>
                                      <br>
                                    <label class="col-md-3">Course Status: </label>
                                    <span><?php if($key->co_status == 1){
                                        echo "Active";
                                      }
                                      else{
                                        echo "In-Active";

                                    } ?></span>
                                   
                               
                                  <?php } ?>
                                  </div>
                            
                              </div>

                          </div>
                      </div>
                  </div> <!-- end col -->
                </div> <!-- end row -->

            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->

