

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Enrolled Student
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
               <!--  <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>enroll_std"><i class="fa fa-plus"></i> Add New</a>
                </div> -->
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">All Enrolled Students</h3>
                    
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">



                                
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Student</th>
                                        <th>Program</th>
                                        <th>Module</th>
                                        <th>Batch</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                       
                                   
                                   <?php
     
     //                   print_r($students);
       //                 die();


        foreach($enroll_student->result() as $enroll_student) {
         
      ?>      
            <tr>   
                            
                <td><?php echo $enroll_student->enrollment_id; ?></td>
                <td><?php echo $enroll_student->fname; ?></td>
                <td><?php echo $enroll_student->program_name; ?></td>
                <td><?php echo $enroll_student->module_name; ?></td>

                <td><a href="<?php echo base_url('enrollestuden') ?>?id=<?php echo $enroll_student->studentid ?>"><?php echo $enroll_student->batch_name; ?></a></td>
                                        

                <td>  
       <!-- <a class="btn btn-sm btn-info" href="<?php echo base_url('show_data_to_update_enroll_student') ?>?id=<?php echo $enroll_student->enrollment_id?>&program=<?php echo $enroll_student->program_id?>&batch=<?php echo $enroll_student->batch_id?>&module=<?php echo $enroll_student->module_id?>&sid=<?php echo $enroll_student->student_id?>" >
        <i class="fa fa-pencil"></i></a> -->
      
        <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url('Main/delete_enroll_students/').$enroll_student->enrollment_id; ?>">
        <i class="fa fa-trash"></i></a>
                </td>

            </tr>
                                   <?php   } ?>

                              
                                    </tbody>
                                </table>











                  
                    
                
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















