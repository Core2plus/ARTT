<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  animationEnabled: true,
 
  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Programs Statistics"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "column", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },
      
    ]
  }]
});
chart.render();
var chart1 = new CanvasJS.Chart("chartContainer1", {
  animationEnabled: true,
 
  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Module  Courses"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "pie", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },
      
    ]
  }]
});
chart1.render();
var chart2 = new CanvasJS.Chart("chartContainer2", {
  animationEnabled: true,
 
  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Student Fee Statistics"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "area", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },
      
    ]
  }]
});
chart2.render();
var chart3 = new CanvasJS.Chart("chartContainer3", {
  animationEnabled: true,
 
  theme: "light1", // "light1", "light2", "dark1", "dark2"
  title:{
    text: "Student Fee Statistics"
  },
  toolTip:{
        enabled: false   //enable here
      },
  data: [{
    type: "area", //change type to bar, line, area, pie, etc
    //indexLabel: "{y}", //Shows y value on all Data Points
    indexLabelFontColor: "#5A5757",
    indexLabelPlacement: "outside",
    dataPoints: [
      { x: 10, y: 71 },
      { x: 20, y: 55 },
      { x: 30, y: 50 },
      { x: 40, y: 65 },
      { x: 50, y: 92},
      { x: 60, y: 68 },
      { x: 70, y: 38 },
      
    ]
  }]
});
chart3.render();
}



</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>Control panel</small>
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <a href="#">
                  <h3 style="color:#fff;"><?php echo @$users;?></h3>
                  <p style="color:#fff;">Users</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
              </a>
                <a href="<?php echo base_url().'stat';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo @$students;?><sup style="font-size: 20px"></sup></h3>
                  <p>Number of Student</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="<?php echo base_url('studentstat') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo @$earning;?> Fee</h3>
                  <p><?php echo @$Rcv_Amt;?> Rec.Fee</p>
                </div>
                <div class="icon">
                  
                </div>
                <a href="<?php echo base_url().'batchstat';?>" class="small-box-footer"><i class="fa fa-arrow-circle-right">CA Fee</i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <div id="chartContainer3" style="height: 84px; width: 100%;"></div>
                </div>
                <div class="icon">
                  
                </div>
                <a href="<?php echo base_url().'stat/get_courses_data1';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <div id="chartContainer" style="height: 84px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                </div>
                <div class="icon">
                  
                </div>
                <a href="<?php echo base_url().'stat/load_course_stat';?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <div id="chartContainer1" style="height: 84px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                </div>
                <div class="icon">
                  
                </div>
                <a href="<?php echo base_url().'charts'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <div id="chartContainer2" style="height: 84px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                </div>
                <div class="icon">
                  
                </div>
                <a href="<?php echo base_url().'drilreport'?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            
    </section>
</div>