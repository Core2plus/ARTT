<style>
    .inv{
        display: none;
    }
    .wrapper.dataTables{
        padding:0px;

    }
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Refund Case
            <small> Approve/Decline </small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <button type="button" class="btn btn-primary" id="modal1"><i class="fa fa-book"></i> Refund Policy </button>
<!--                    <a class="btn btn-primary" href="--><?php //echo base_url().'charts';?><!--"><i class="fa fa-pie-chart"></i>Chart/ Records</a>-->
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">All Cases</h3>

                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive ">




                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ARTT ID</th>
                                <th>CR#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Batch Name</th>
                                <th>Phone</th>




                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no = 1;
                            if(@$students){
                                foreach ($students->result() as $student) {

                                    ?>

                                    <tr>



                                        <td><?php echo $no++;  ?></td>

                                        <td><?php echo $student->artt_id;   ?> </td>
                                        <td><?php echo $student->cr;   ?> </td>
                                        <td> <?php echo $student->fname;  ?> </td>
                                        <td> <?php echo $student->email;  ?> </td>
                                        <td style = "width : 20px" > <?php
                                            if($student->fstatus=="UnFreeze")
                                            {

                                                echo "Regular";
                                            }
                                            if($student->fstatus=="Freeze")
                                            {

                                                echo "Freezed";
                                            }

                                            ?>

                                        </td>
                                        <td>
                                            <span style = "font-size : 14px; ">

                                                    <a href="user/refundcase/<?php echo $student->artt_id;   ?>">
                                                <?php echo $student->batch_name;  ?>
                                            </span>
                                        </td>

                                        <td>
                                            <?php echo $student->phone;  ?>
                                        </td>





                                        <td class="text-center">

                                            <a class="btn btn-sm btn-info" href="#" title="Edit"><i class="fa fa-pencil"></i> </a>


                                        </td>

                                    </tr>




                                <?php }
                            }?>
                            </tbody>

                        </table>

                    </div>

                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div>
</section>
</div>
<script>

    function myfun(e)
    {

        p=document.getElementById("paragraph"+e);
        $(p).toggle("fade");

    }

</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });


    $(document).ready(function() {
        $('#example1').dataTable( {
            // "sDom": '<"wrapper"><lf<t>pi>',
            "iDisplayLength": 5,
            "aLengthMenu": [[5, 10, 25, -1], [5, 10, 25, "All"]]

        } );
    } );

</script>










<div id="myModal" class="modal fade" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>


            <div class="modal-body">


<p style="font-size:large;"> you are not 100% satisfied with your purchase, you can return the product and get a full refund or exchange the product for another one, be it similar or not. You can return a product for up to 30 days from the date you purchased it.</p>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

            </div>
        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>

    $('#modal1').click(function () {
        $('#myModal').modal('show');
        $('#myModal').find('.modal-title').text('Refund Policy');
        $('#myModal').modal({
            backdrop: 'static',
            keyboard: false
        })
    });

</script>