<?php
 
$totalVisitors = 883000;
 if(@$program)
 {
    $i=1;
    foreach($program as $k=>$v)
    {

        $newVsReturningVisitorsDataPoints[]=array("y"=>$v['program_id'],"name"=>$v['program_name'],"id"=>$v['program_id']);


        $i++;


    }


 }

 
$newVisitorsDataPoints = array(
    array("x"=> 1420050600000 , "y"=> 33000),
    array("x"=> 1422729000000 , "y"=> 35960),
    array("x"=> 1425148200000 , "y"=> 42160),
    array("x"=> 1427826600000 , "y"=> 42240),
    array("x"=> 1430418600000 , "y"=> 43200),
    array("x"=> 1433097000000 , "y"=> 40600),
    array("x"=> 1435689000000 , "y"=> 42560),
    array("x"=> 1438367400000 , "y"=> 44280),
    array("x"=> 1441045800000 , "y"=> 44800),
    array("x"=> 1443637800000 , "y"=> 48720),
    array("x"=> 1446316200000 , "y"=> 50840),
    array("x"=> 1448908200000 , "y"=> 51600)
);
 
$returningVisitorsDataPoints = array(
    array("x"=> 1420050600000 , "y"=> 22000),
    array("x"=> 1422729000000 , "y"=> 26040),
    array("x"=> 1425148200000 , "y"=> 25840),
    array("x"=> 1427826600000 , "y"=> 23760),
    array("x"=> 1430418600000 , "y"=> 28800),
    array("x"=> 1433097000000 , "y"=> 29400),
    array("x"=> 1435689000000 , "y"=> 33440),
    array("x"=> 1438367400000 , "y"=> 37720),
    array("x"=> 1441045800000 , "y"=> 35200),
    array("x"=> 1443637800000 , "y"=> 35280),
    array("x"=> 1446316200000 , "y"=> 31160),
    array("x"=> 1448908200000 , "y"=> 34400)
);
 
?>

<script>
window.onload = function () {
 
var totalVisitors = <?php echo $totalVisitors ?>;
var visitorsData = {
    "New vs Returning Visitors": [{
        click: visitorsChartDrilldownHandler,
        cursor: "pointer",
        legendText: "{name}",
        explodeOnClick: false,
        innerRadius: "75%",
        legendMarkerType: "square",
        indexLabel: "{name}",
        name: "New vs Returning Visitors",
        radius: "100%",
         toolTip:{
        enabled: false,       //disable here
        animationEnabled: true //disable here
      },
  
        showInLegend: true,
        startAngle: 90,
        type: "doughnut",
        dataPoints: <?php echo json_encode($newVsReturningVisitorsDataPoints, JSON_NUMERIC_CHECK); ?>
    }


    ],
   

    "New Visitors": [{
        color: "#E7823A",
        name: "New Visitors",
        type: "column",
        xValueType: "dateTime",
        dataPoints: <?php echo json_encode($newVisitorsDataPoints, JSON_NUMERIC_CHECK); ?>
    }],
    "Returning Visitors": [{
        color: "#546BC1",
        name: "Returning Visitors",
        type: "column",
        xValueType: "dateTime",
        dataPoints: <?php echo json_encode($returningVisitorsDataPoints, JSON_NUMERIC_CHECK); ?>
    }]
};
 
var newVSReturningVisitorsOptions = {
    animationEnabled: true,
     toolTip:{
        enabled: false,       //disable here
        animationEnabled: true //disable here
      },
    theme: "light2",
    title: {
        text: "Programs"
    },
    subtitles: [{
        text: "Click on Any Segment to Drilldown",
        backgroundColor: "#2eacd1",
        fontSize: 16,
        fontColor: "white",
        padding: 5
    }],
    legend: {
        fontFamily: "calibri",
        fontSize: 14,
        itemTextFormatter: function (e) {
            return e.dataPoint.name + ": " + Math.round(e.dataPoint.y / totalVisitors * 100) + "%";  
        }
    },
    data: []
};
 
var visitorsDrilldownedChartOptions = {
    animationEnabled: true,
    theme: "light2",
    axisX: {
        labelFontColor: "#717171",
        lineColor: "#a2a2a2",
        tickColor: "#a2a2a2"
    },
    subtitles: [{
        text: "Click on Any Segment to Drilldown",
    }],
    axisY: {
        gridThickness: 0,
        includeZero: false,
        labelFontColor: "#717171",
        lineColor: "#a2a2a2",
        tickColor: "#a2a2a2",
        lineThickness: 1
    },
    data: []
};
 
var chart = new CanvasJS.Chart("chartContainer", newVSReturningVisitorsOptions);
chart.options.data = visitorsData["New vs Returning Visitors"];
chart.render();
 
function visitorsChartDrilldownHandler(e) {

    chart = new CanvasJS.Chart("chartContainer", visitorsDrilldownedChartOptions);
    var param="id="+e.dataPoint.y;
    var module;
    
         /*   var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var chart = new CanvasJS.Chart("chartContainer", {
    theme: "light2",
    animationEnabled: true,
    title: {
        text: "Modules"
    },
    
    data: [{
        type: "doughnut",
        indexLabel: "{name}",
        showInLegend: true,
        legendText: "{name}",
        click:gettabledata,
        dataPoints: JSON.parse(this.responseText)
    }]
});
        alert(this.responseText);
chart.render();
 

    $("#backButton").toggleClass("invisible");
      
    }
  };

  xhttp.open("POST", "<?php //echo base_url().'Stat/load_data_by_program';?>");
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);*/
$.ajax({
        type:"POST",
        cache:false,
        url:"<?php echo base_url().'Stat/load_data_by_program';?>",
        data:param,    // multiple data sent using ajax
        success: function (html) {
            var chart = new CanvasJS.Chart("chartContainer", {
    theme: "light2",
    animationEnabled: true,
    title: {
        text: "Modules"
    },
    exportEnabled: true,
    
    data: [{
        showInLegend: "true",
        type: "doughnut",
        indexLabel: "{name}",
        showInLegend: true,
        legendText: "{name-} ",
        click:gettabledata,
        dataPoints: JSON.parse(html)
    }]
});
        
chart.render();
 

    $("#backButton").toggleClass("invisible");

            

        }
      });

 
}
$("#backButton").click(function() { 
    $(this).toggleClass("invisible");
    chart = new CanvasJS.Chart("chartContainer", newVSReturningVisitorsOptions);
    chart.options.data = visitorsData["New vs Returning Visitors"];
    chart.render();
});
 
}
function gettabledata(e){
var myNode = document.getElementById("table");
while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
} 
    var param="id="+e.dataPoint.id;
   
    $.ajax({
        type:"POST",
        cache:false,
        url:"<?php echo base_url().'Stat/load_data_by_module';?>",
        data:param,    // multiple data sent using ajax
        success: function (html) {
            myNode.innerHTML=html;
            



          
        }
      });


    
    
    
}
</script>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Programs Info. 
        <small>Chart and data</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
  
 
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<button class="btn invisible" id="backButton">&lt; Back</button>
<div class="box-body table-responsive" style="max-height:500px;">
    <table class="table table-striped table-bordered" >
        <thead>
            <tr><th>S.No</th><th>Course Name</th><th>Course Code</th><th>Course Fee</th></tr>
        </thead>
        <tbody id="table">
        </tbody>
    </table>

    </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>

<script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
