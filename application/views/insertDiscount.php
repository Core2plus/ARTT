<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     <h1>
        <i class="fa fa-users"></i> Discount
        <small>Add Discount Percent</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="col-md-8"  style="margin-left: 15%; margin-top: 6%;">
      <div class="box">
        
        <div class="box-body">
                  <div class="row">
                    
                        <div class="box-body table-responsive">
                            <?php  foreach($discount_student->result() as $key) {?>
                                              
                                <form action="<?php echo base_url('insert_discount') ?>" method="post">
                                       
                                     <div class="col-md-4">

                                      <input type="hidden" name="id" value="<?php echo $key->studentid; ?>">
                                    
                                        <label>ARTT_ID<span class="required">*</span></label>
                                        
                                    
                                            
                                        <input type="text"  disabled value="<?php echo $key->artt_id;?>" class="form-control">

                                    
                                        </div>
                                         
                                     
                                      <div class="col-md-4">
                                   
                                        <label>Student Name<span class="required">*</span></label>
                                        
                                        <div>
                                            
                                        <input type="text" name="fname"  disabled value="<?php echo $key->fname;?>" class="form-control">
                                    
                                        </div>
                                        </div>
                                    <?php } ?>
                                    
                                            
                                            
                                               
                                    <?php  foreach($discount_batch->result() as $key2) {?>

                                      <div class="col-md-4">
                                      <label>Batch<span class="required">*</span></label>
                                        
                                        <div>
                                        <input type="hidden" name="batch_id" value="<?php echo $key2->batch_id; ?>">

                                    <input type="text" name="batch_name"  disabled value="<?php echo $key2->batch_name;?>" class="form-control">
                                    </div>
                                    </div>

                                    <?php
                                        break;}                                    ?>
                                           
                                        

                                
                                         <div class="col-md-4">
                                      <label>Total Fees<span class="required">*</span></label>
                                        
                                   
                                                                        
                                    <?php $c=0; foreach($discount_batch->result() as $key3) { $c=$c+$key3->coursefee;?>

                                    

                                    <?php
                                        }    
                                         ?>
                                        <input type="text" id="input1"   disabled value="<?php echo $c; ?>" class="form-control">
                                           
                                        </div>
                                         <div class="col-md-4">
                                        <label>Percentage<span class="required">*</span></label>
                                       
                                            <input type="number" id="input2" maxlength='100' max="100" name="percentage"  class="form-control">
                                    
                                        </div>
                                          <div class="col-md-4">
                                        <label>discount amount<span class="required">*</span></label>
                                       
                                            <input type="number" disabled name="output" id="output" placeholder="discount amount " class="form-control">
                                    
                                        </div>
                                    
                                   


                                                                      
                                   <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Note</label>
                                        
                                            <textarea class="form-control form-group" name="note" placeholder="Enter Your Reason.."></textarea>
                                        </div>
                                    
                                </div><br>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        
                                    <?php $batch=$discount_batch->result_array();
                                        if($batch[0]['discount_per']!=''|| $batch[0]['discount_per']!=0)
                                          {
                                            ?>
                                    <button  disabled="true" type="submit" class="btn btn-primary waves-effect waves-light" style="width: 15%;">
                                                Discounted :<?php echo $batch[0]['discount_per'].'%';?>
                                            </button>
                                            <?php
                                          }
                                          else{
                                            ?>
                                            <button  type="submit" class="btn btn-primary waves-effect waves-light" style="width: 15%;">
                                                Submit
                                            </button>
                                            <?php
                                          }
                                          ?>
                                            
                                        </div>
                                    </div>


                                </form>

                            
                        </div>
                    </div> <!-- end col -->

        
        <!-- /.box-body -->
        
        <!-- /.box-footer-->
      </div>
      <!-- /.box --></div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <script>
$("#input2,#input1").keyup(function () {
    $('#output').val(($('#input1').val() * $('#input2').val()) /100);
});

function calc(){
   var textValue1 = document.getElementById('input1').value;
   var textValue2 = document.getElementById('input2').value;

   document.getElementById('output').value = (textValue1 * textValue2) ; 
 }

</script>