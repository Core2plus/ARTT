
<!DOCTYPE html>
<html>

<?php
if (isset($this->session->userdata['logged_in'])) {
?>

<head>
	<title> Comparison Lab Report </title>
</head>
<body>

<div class="col-md-12">

	<img src="<?php base_url(); ?>assets/assets/img/logo.jpg" alt="" style="margin-left: 15%;">

	<?php foreach ($headinfo as $u_key){ ?>
	<h2 style="color: green; padding-left: 30%;    margin-top: -3%;"> <?php echo $u_key->main_title; ?> </h2>

</div>

<hr style="border: 1px solid black">
<hr style="border: 3px solid black">

<p style="margin-left: 20%;font-size: 16px;    margin-bottom: -1%;"><?php echo $u_key->Hno; ?><?php echo $u_key->streetNo; ?><?php echo $u_key->sectorNo; ?><?php echo $u_key->city; ?><?php echo $u_key->telephone; ?><?php echo $u_key->fax; ?></p>
<p style="margin-left: 21%;"><?php echo $u_key->email; ?><?php echo $u_key->website; ?></p>

<?php } ?>

<table style="    border: 2px solid #000;width: 100%;">
	<tbody>
	<?php foreach ($master as $u_key){ ?>
		<tr>
			<td style="text-align: left">MR # :</td>
			<td style="text-align: left"><?= $u_key->mrno; ?></td>
			<td style="text-align: left">Lab. # :</td>
			<td style="text-align: left"><?= $u_key->labno; ?></td>
		</tr>
		<tr>
			<td style="text-align: left">Patient Name :</td>
			<td style="text-align: left"><?= $u_key->patient_name; ?></td>
			<td style="text-align: left">Date :</td>
			<td style="text-align: left"><?= $u_key->report_date; ?></td>
		</tr>
		<tr>
			<td style="text-align: left">Employer Name :</td>
			<td style="text-align: left"><?= $u_key->employer; ?></td>
			<td style="text-align: left">DOB :</td>
			<td style="text-align: left"><?= $u_key->dob; ?></td>
		</tr>
		<tr>
			<td style="text-align: left">Gender :</td>
			<td style="text-align: left"><?= $u_key->gender; ?></td>
			<td style="text-align: left">Age :</td>
			<td style="text-align: left"><?= $u_key->age; ?></td>
		</tr>

	<?php }?>

	</tbody>
</table>

<h2 style="color: blue; margin-left: 32%">LABORATORY REPORT</h2>
<?php foreach ($master as $u_key) { ?>
<table style="    border: 2px solid #000;width: 100%;background-color: teal;">
	<tbody>

	<tr>
		<td style="color: white;font-family: initial ;text-align: left;">Sr. #</td>
		<td style="color: white;font-family: initial;text-align: left;">Test Name</td>
		<td style="color: white;font-family: initial;text-align: left;">  <?= $u_key->test_date1; ?>   </td>
		<td style="color: white;font-family: initial;text-align: left;">  <?= $u_key->test_date2; ?>   </td>
		<td style="color: white;font-family: initial;text-align: left;">  <?= $u_key->test_date3; ?>   </td>
		<td style="color: white;font-family: initial;text-align: left;">Unit Value</td>
		<td style="color: white;font-family: initial;text-align: left;">Normal Range</td>
	</tr>
	</tbody>
</table>

<h3><a href="#" style="color: #0f0f0f; text-decoration: underline;font-size: 12px;margin-left: 10%"><?= $u_key->category; ?></a></h3>

<table style="width: 100%; margin-left: 5%;    padding-left: 5%;">
	<tbody>
		<tr>
			<td style="font-family: initial ;text-align: left;"><?= $u_key->pid; ?></td>
			<td style="font-family: initial;text-align: left;"><?= $u_key->test_name; ?></td>
			<td style="font-family: initial;text-align: left;"><?= $u_key->past_result1; ?></td>
			<td style="font-family: initial;text-align: left;"><?= $u_key->past_result2; ?></td>
			<td style="font-family: initial;text-align: left;"><?= $u_key->past_result3; ?></td>
			<td style="font-family: initial;text-align: left;"><?= $u_key->unit; ?></td>
			<td style="font-family: initial;text-align: left;"><?= $u_key->normal; ?></td>
		</tr>
	<?php }?>

	</tbody>
</table>
<p><b>Remarks :</b></p>
<p style="margin-bottom: 10%">**</p>

<hr style="border: 1px solid black">
<br>
<br>

<div class="col-md-12">
	<img src="<?php base_url(); ?>assets/assets/img/img1.jpg"  alt="" style="margin-left: 3%;    height: 60px;
    width: 60px;">
	<img src="<?php base_url(); ?>assets/assets/img/img2.jpg"  alt="" style="padding-left: 77%;    height: 60px;
    width: 60px;">

</div>


<?php foreach ($footinfo as $row): ?>
<p style="margin-left: 100px;    margin-top: -1%;%">This is a verified computer generated report. It does not need a stamp or a signature.</p><br />
<p style="margin-left: 100px;    margin-top: -1%;%"><em><?= $row->disclaimer; ?></em></p>

<p style="    margin-left: 60%;"><?= $row->report_no; ?> <nbsp> <nbsp></nbsp></nbsp> <?= $row->registration_no; ?></p>

<?php endforeach; ?>





</body>
</html>


<?php
} else {
	header(base_url()); }

?>
