

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> All Batches
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url() ?>addbatch"><i class="fa fa-plus"></i> Add New</a>
                    <a class="btn btn-primary" href="<?php echo base_url() .'batchstat';?>"><i class="fa fa-plus"></i> Charts And Data</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Batch List</h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">




                     <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                                        <th>Program</th>
                                        <th>Batch</th>
                                        <th>Module</th>
                                        <th>Start</th>
                                        <th>Status</th>
                                        
                                       
                        
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>
                <tbody>
                 <?php
                  $no = 1;
                   foreach ($show_batch as $show) {
                     
                   ?>
               
                <tr>
                   <td><?php echo $no++; ?></td>
                                        <td><?php echo $show->program_name; ?></td> 
                                        <td><?php echo $show->batch_name; ?></td>
                                         <td><?php echo $show->module_name; ?></td>
                                        <td><?php echo $show->startdate; ?></td>
                                        <td><?php if($show->status == 1){
                                            echo "Active";
                                        }
                                        else{
                                            echo "In-Active";
                                        } ?></td>
                                        
                        <td class="text-center">
                                                                  
        <a class="btn btn-sm btn-info" href="<?php echo base_url('editbatch'); ?>?id=<?php echo $show->batch_id; ?>"
          title="Edit"><i class="fa fa-pencil"></i></a>
        <a class="btn btn-sm btn-danger deleteUser" href="<?php echo base_url('deletebatch'); ?>?id=<?php echo $show->batch_id; ?>" data-userid="" title="Delete"><i class="fa fa-trash"></i></a>
                        </td>
                  
                </tr>
              
 
               <?php } ?>
                </tbody>
                
              </table>











                  
                    
                
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















