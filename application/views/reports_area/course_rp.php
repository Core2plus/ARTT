
<!DOCTYPE html>
<html>

<head>
    <title> <?= $title ?> </title>
</head>
<body>

<div class="col-md-12">

    <?php foreach ($cinfo as $u_key){ ?>
    <img src="<?php base_url(); ?>assets/images/faviicon.png" style="height: 60px; width: 60px; margin-top: 1%;">
    <h1 style="color: green; text-align: center;    margin-top: -6%;"> <?php echo $u_key->companyName; ?>  </h1>

    <?php date_default_timezone_set("Asia/Karachi"); ?>
    <p style="text-align: right; margin-left: 8%; font-size: 12px;"><?= date("h:i:sa"); ?> <?= date('d-m-Y'); ?></p>


    <p style="text-align: center; margin-top: -120px; font-weight: bold;"><?= $title ?>    </p>
    <hr>

</div>

<section class="content">
    <div class="row"  >
        <div class="col-sm-12" >
            <div class="box">
                <div class="box-body">
                    <div style="overflow-x:auto;">
                        <table id="example2" class="table table-bordered table-hover" style="width: 100%; ">
                            <thead>
                            <tr style="background-color: #e0e7f7">
                                <th>NO</th>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>Total Students Enrolled</th>
                                <th>Total Freeze Students</th>
                                <th>Total Unfreeze Students</th>
                                <th>Total Course Amount</th>
                                <th>Total Freeze Amount</th>
                                <th>Total UnFreeze Amount</th>
                                <th>Total Discount Amount</th>
                                <th>Received Amount</th>
                                <th>Balance Amount</th>


                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $no=1;
                            $a=0;
                            $b=0;
                            $c=0;
                            $f=0;

                            foreach($result as $show) {
                                ?>

                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td> <?= $show->coursename;   ?> </td>
                                    <td><?= $show->coursecode;   ?></td>
                                   <td><?= $show->Total_Student; ?> </td>
                                    <td><?= $show->Frz_Student; ?> </td>
                                    <td><?= $show->UnFrz_Student; ?> </td>
                                    <td><?=  $show->Total_CourseFee; ?></td>
                                    <td><?=  $show->Frz_Rcv_Amt; ?></td>
                                    <td><?=  $show->UnFrz_Rcv_Amt; ?></td>
                                    <td><?=  $show->Discount_Amt; ?></td>
                                    <td><?= $show->TotRcvFeeAmt; ?></td>
                                    <td style="font-weight: bold; border-style: dotted;"><?= $show->Bal_2; ?></td>


                                </tr>

                            <?php }?>
                            </tbody>
                        </table><br>
                    </div></div></div></div>
        <!-- /.row -->

</section>

<!--<hr style="border: 3px solid black; margin-top: -4px;">-->

<div style="z-index: 100; position: fixed; bottom: 38px;">
    <hr>
    <p style="font-size: 13px;    margin-bottom: -1%; text-align: center"><?php echo $u_key->aboutCompany; ?></p>
    <p style="text-align: center; font-size: 13px; padding-top: -5px;"><?php echo $u_key->website;?>&nbsp;&nbsp;<?php echo $u_key->contactNo; ?>&nbsp;&nbsp;<?php echo $u_key->address; ?>&nbsp;&nbsp;<?php echo $u_key->email; ?></p>
</div>

<?php } ?>

</body>
</html>
