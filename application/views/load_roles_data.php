<?php //include('includes/header.php');?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Roles Of Employess 
        <small>Chart and data</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <?php
              foreach($roles as $k=>$v)
              {
                $dataPoints[]= array('label'=>$v['type'],'y'=>$v['num']);


              }


    

    
?>
  
<script>
window.onload = function () {
 
var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    exportEnabled: true,
    title:{
        text: "Roles of Employees"
    },
    subtitles: [{
        text: "Total of Employees "+<?php  if(@$total){echo $total;}else{ echo "0";}?>
    }],
    data: [{
        click: function(e){
            var param="type="+e.dataPoint.label;
            var xhttp = new XMLHttpRequest();
           var myNode = document.getElementById("body");
while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
} 
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        
      myNode.innerHTML=this.responseText;
    }
  };

  xhttp.open("POST", "<?php echo base_url().'Stat/load_data_sp_type';?>");
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);

        
        },
        type: "pie",
        showInLegend: "true",
        legendText: "{label}",
        indexLabelFontSize: 16,
        indexLabel: "{label} - #percent%",
        yValueFormatString: "#,##0",
        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    }]
});
chart.render();
 
}
</script>

<div id="chartContainer" style="height: 300px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<?php 

if(@$datas)
{
    $i=1;
    ?>
    <div class="box-body table-responsive" style="max-height:500px;">
        <table class="table table-striped table-bordered" id="example1">
            <thead>
                
                <tr>
                    
                    <th>S.No</th>
                    <th>Name</th>
                    <th>Roles</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Created Date</th>
                </tr>


            </thead>
            <tbody id="body">
<?php
foreach($datas as $k=>$v)
{
    ?>
    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $v['NAME'];?></td>
        <td><?php echo $v['role'];?></td>
        <td><?php echo $v['email'];?></td>
        <td><?php echo $v['mobile'];?></td>
        <td><?php echo $v['createddtm'];?></td>
        </tr>

    <?php
   

$i++;

}
}
?>
</tbody>
</table>

            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>