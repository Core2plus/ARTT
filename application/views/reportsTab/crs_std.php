
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-tachometer" aria-hidden="true"></i> Reports Management
            <small>View Reports</small>
        </h1>
    </section>
    <section class="content-header">
        <img src="<?php echo base_url()."assets/images/"; ?>ARTT_BS2.png" class="img-responsive center-block" style="width: 45%; min-width: 300px;  height: 45%  alt=" header-logo">
        <!--<h1>-->

        <br>
        <br>
        <form method="post" action="<?php echo base_url()."reports/course_based_report"; ?>" target="_blank">


            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-12">

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-12">

                        <label style="font-size: 16px;">Select Batch/Year</label>

                        <select onchange="myfun()" name="batch" id="batch" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                            <option value="">Select Batch</option>


                            <?php
                            foreach($batch as $row)
                            {
                                echo '<option value="'.$row->batch_id.'">'.$row->batch_name.'</option>';
                            }
                            ?>


                        </select>
                        <br>


                        <label style="font-size: 16px;">Select Course</label>
                        <select name="course" id="course" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                            <option value="">Select Course</option>

                        </select>
                        <br>  <br>
                        <input type="submit" value="Show Report" class="btn btn-block btn-primary btn-md" style="width: 100%">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-9">

                    </div>
                </div>
            </div>

        </form>


        <?php

        if($this->session->flashdata('message_name') !== null)
            echo "<p class=\"text-center bg-success text-success\" style=\"padding: 5px;font-size:14px; font-weight: 600;\" >" . $this->session->flashdata('message_name') . "</p>";
        ?>


        <?php
        if($this->session->flashdata('msg_to_user') !== null)
            echo "<p class=\"text-center bg-success text-success\" style=\"padding: 5px;font-size:14px; font-weight: 600;\" >" . $this->session->flashdata('msg_to_user') . "</p>";
        ?>



    </section>

</div>


<script>
    function myfun() {
        batch_id=document.getElementById('batch').value;
        param="batch_id="+batch_id;
        xml=new XMLHttpRequest();
        xml.onreadystatechange=function(){
            if(xml.status==200 && xml.readyState==4)
            {



                document.getElementById("course").innerHTML=(xml.responseText);


            }


        };
        xml.open("POST","<?php echo base_url().'reports/fetch_course';?>");
        //xml.setRequestHeader("");
        xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xml.send(param);

        //$.ajax({
        //    url: "<?php //echo base_url('reports/fetch_course'); ?>//",
        //    method: "POST",
        //    data: {batch_id: batch_id},
        //    success: function (data) {
        //        $('#course').html(data);
        //
        //    }
        //});

    }
        //alert("hello");



    //$(document).ready(function(){
    //    $('#batch').change(function(){
    //        var batch_id = $('#batch').val();
    //        if(batch_id != null)
    //        {
    //
    //        }
    //        else
    //        {
    //            $('#course').html('<option value="">Select course</option>');
    //
    //        }
    //    });
    //
    //});
</script>
