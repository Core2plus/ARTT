

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-tachometer" aria-hidden="true"></i> Reports Management
            <small>View Reports</small>
        </h1>
    </section>
    <section class="content-header">
        <img src="<?php echo base_url()."assets/images/"; ?>ARTT_BS2.png" class="img-responsive center-block" style="width: 45%; min-width: 300px;  height: 30%  alt="header-logo">
        <!--<h1>-->

        <br>
        <br>


        <form method="post" action="<?php echo base_url()."reports/singlestd_report"; ?>" target="_blank">


        <div class="row">
        
            <div class="col-md-4">
                <div class="col-md-12">

                </div>
            </div>
   
                <div class="col-md-4" style="margin-left: -390px; float: left; margin-top: -30px;">
                    <div class="col-md-12">

<center><label style="font-size: 20px;"><u>STUDENTS REPORT</u></label></center><br>
                  <label style="font-size: 16px;">Select Batch/Year</label>
                    <select name="batchname" id="" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                        <option value="">Select</option>
                        <option value="all">All</option>




           
            </select>
            <br>
            <label style="font-size: 16px;">Student ID/Roll No</label>

                        <div class="row">
                            <div class="col-md-6">
                                <label style="font-size: 16px;">From</label>
                            <input type="text" name="artt_id_from" placeholder="ARTT-ID" class="form-control" maxlength="9"  required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
                            </div>
                            <div class="col-md-6"> <label style="font-size: 16px;">To</label>

                                <input type="text" name="artt_id_to" placeholder="ARTT-ID" class="form-control" maxlength="9"  required oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
                            </div>
                        </div>

                        <br> <br><br>
 <input type="submit" value="Show Report" class="btn btn-block btn-primary" style="width: 100%">
            </div>
                </div></form>


<div style="height: 335px; width: 1px; float: left; background-color:  black; margin-top: -30px;  margin-left: -16px;"></div>

                 <div class="col-md-4" style="margin-top: -30px; margin-left: -2%;" >
                    <div class="col-md-12"><form method="post" action="<?php echo base_url()."reports/student_report"; ?>" target="_blank">
<center><label style="font-size: 20px;"><u>STATUS REPORT</u></label></center><br>
<label style="font-size: 16px;">Select Batch/Year</label>

                    <select name="batchname" id="" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                        <option value="">Select</option>
                        <option value="all">All</option>

                        <?php if(@$batch1)
                {

                    foreach($batch1 as $k=>$v)

                        {
                            ?>
                            <option value="<?= $v['batch_id'];?>"><?= $v['batch_name'];?></option>

                            <?php
                        }
                }
                ?>

            </select>
            <br>
            <label style="font-size: 16px; margin-top: 9%;">Select Status</label>
             <select name="report_type" id="" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                            <option value="">Select</option>
                            <option value="p">Paid</option>
                            <option value="u">Unpaid</option>
                            <option value="f">Freeze</option>
                            <option value="uf">UnFreeze</option>
                        </select>
<br> <br>
<input type="submit" value="Show Report" class="btn btn-block btn-primary btn-md" style="width: 100%; margin-top: 1.8%;">
            </div>
                </div></form>

<div style="height: 335px; width: 1px; float: left; background-color:  black; margin-top: -30px; "></div>
                <div class="col-md-4" style="margin-top: -30px;" >
                    <div class="col-md-12">
<form method="post" action="<?php echo base_url()."reports/course_based_report"; ?>" target="_blank">
    <center><label style="font-size: 20px;"><u>COURSE REPORT</u></label></center><br>
                        <label style="font-size: 16px;">Select Batch/Year</label>

                        <select onchange="myfun()" name="batch" id="batch" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 3%;" required>
                            <option value="">Select Batch</option>


                            <?php
                            foreach($batch as $row)
                            {
                                echo '<option value="'.$row->batch_id.'">'.$row->batch_name.'</option>';
                            }
                            ?>


                        </select>
                        <br>


                        <label style="font-size: 16px; margin-top: 10%;">Select Course</label>
                        <select name="course" id="course" class="btn btn-block btn-default btn-md" style="width: 100%; margin-bottom: 4%;" required>
                            <option value="">Select Course</option>

                        </select>
                        <br>  <br>
                        <input type="submit" value="Show Report" class="btn btn-block btn-primary btn-md" style="width: 100%; margin-top: 1.8%;">
                    </div>
                </div>

<!--            <div class="col-md-2">-->
<!--                <div class="form-group has-feedback">-->
<!--                    <input type="text" name="batch" class="form-control" placeholder="Batch">-->
<!--                    <span class="glyphicon glyphicon-user form-control-feedback"></span>-->
<!--                </div>-->
<!--            </div>-->

<!--            <div class="col-md-2">-->
<!--                <div class="form-group">-->
<!---->
<!--                    <div class="input-group date">-->
<!--                        <div class="input-group-addon">-->
<!--                            <i class="fa fa-calendar"></i>-->
<!--                        </div>-->
<!--                        <input type="text" name="date_value" class="form-control pull-right" id="datepicker" required>-->
<!--                    </div>-->
<!--                    <!-- /.input group -->
<!--                </div>-->
<!--            </div>-->


            <div class="col-md-4" >
                <div class="col-md-9">
                    
                </div>
            </div>
        </div>

                </form>


                <?php

        if($this->session->flashdata('message_name') !== null)
            echo "<p class=\"text-center bg-success text-success\" style=\"padding: 5px;font-size:14px; font-weight: 600;\" >" . $this->session->flashdata('message_name') . "</p>";
        ?>


        <?php
        if($this->session->flashdata('msg_to_user') !== null)
            echo "<p class=\"text-center bg-success text-success\" style=\"padding: 5px;font-size:14px; font-weight: 600;\" >" . $this->session->flashdata('msg_to_user') . "</p>";
        ?>



    </section>
    <br>
    <!---->
    <!--<script>-->
    <!--	$(document).ready(function() {-->
    <!--	$('#example2').DataTable( {-->
    <!--	"order": [[ 5, "desc" ]]-->
    <!--	} );-->
    <!--	} );-->
    <!--</script>-->

    <!-- Main content -->
    <!-- /.content -->

</div>

<script>
    function myfun() {
        batch_id=document.getElementById('batch').value;
        param="batch_id="+batch_id;
        xml=new XMLHttpRequest();
        xml.onreadystatechange=function(){
            if(xml.status==200 && xml.readyState==4)
            {



                document.getElementById("course").innerHTML=(xml.responseText);


            }


        };
        xml.open("POST","<?php echo base_url().'reports/fetch_course';?>");
        //xml.setRequestHeader("");
        xml.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xml.send(param);

        //$.ajax({
        //    url: "<?php //echo base_url('reports/fetch_course'); ?>//",
        //    method: "POST",
        //    data: {batch_id: batch_id},
        //    success: function (data) {
        //        $('#course').html(data);
        //
        //    }
        //});

    }
        //alert("hello");



    //$(document).ready(function(){
    //    $('#batch').change(function(){
    //        var batch_id = $('#batch').val();
    //        if(batch_id != null)
    //        {
    //
    //        }
    //        else
    //        {
    //            $('#course').html('<option value="">Select course</option>');
    //
    //        }
    //    });
    //
    //});
</script>