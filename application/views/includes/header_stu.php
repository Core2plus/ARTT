<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo "Bussiness School"; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/faviicon.png">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- FontAwesome 4.3.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="<?php echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
    <style>
      .error{
        color:red;
        font-weight: normal;
      }


    </style>

    <style>
  #backButton {
    border-radius: 4px;
    padding: 8px;
    border: none;
    font-size: 16px;
    background-color: #2eacd1;
    color: white;
    position: absolute;
    top: 10px;
    left: 50px;
    cursor: pointer;

  }
  .invisible {
    display: none;
  }
  .lefted
  {
    float:left;
    margin-left:20px;

  }
</style>

    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">
      
      <header class="main-header">
        
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <div class="navbar-header">
      <button style="color:#fff;background-color: red" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar" style="color:#fff;"></span>
        <span class="icon-bar" style="color:#fff;"></span>
        <span class="icon-bar" style="color:#fff;"></span> 
      </button>
    </div>


          <!-- Sidebar toggle button-->
          
          <div class="collapse navbar-collapse" id="myNavbar">
          <div class="">
            <ul class="nav navbar-nav">
             
              <li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
              <li class="dropdown treeview" >
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-book"></i> <span>Main</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="dropdown-menu treeview-menu">
            <li><a href="<?php echo base_url(); ?>program"><i class="fa fa-circle-o"></i> Program </a></li>
            <li><a href="<?php echo base_url(); ?>module"><i class="fa fa-circle-o"></i> Modules </a></li>
            <li><a href="<?php echo base_url('course') ?>"><i class="fa fa-circle-o"></i> Courses </a></li>
            <li><a href="<?php echo base_url('batch') ?>"><i class="fa fa-circle-o"></i> Batches </a></li>
          </ul>
        </li>
        <li class="dropdown treeview">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-fw fa-odnoklassniki"></i> <span>Student</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="dropdown-menu treeview-menu">
            <li><a href="<?php echo base_url('student') ?>"><i class="fa fa-circle-o"></i> Student </a></li>
            <li><a href="<?php echo base_url('show_all_enrolled_students'); ?>"><i class="fa fa-circle-o"></i> Enrolle Student </a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Student Discount</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Report </a></li>
          </ul>
        </li>
        <li class="treeview">
              <a href="<?php echo base_url(); ?>userListing">
                <i class="fa fa-users"></i>
                <span>Users</span>
              </a>
            </li>

          </ul> 

            <ul class="nav navbar-nav navbar-right user user-menu" style="padding-right:10px;" >
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                  <i class="fa fa-history"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"> Last Login : <i class="fa fa-clock-o"></i> <?= empty($last_login) ? "First Time Login" : $last_login; ?></li>
                </ul>
              </li>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    
                    <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $name; ?>
                      <small><?php echo $role_text; ?></small>
                    </p>
                    
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url(); ?>profile" class="btn btn-warning btn-flat"><i class="fa fa-user-circle"></i> Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
               
      
    </ul>
       


              <!-- User Account: style can be found in dropdown.less -->
                 <!-- Menu Footer-->
                  
                </ul>
              </li>
            </ul>
          </div>
        </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      