

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Voucher Reports
        
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                   
                </div><!-- /.box-header -->
                <div class="box-body table-responsive ">


<a href="<?php echo base_url('show_paid_reports_bko_for_print'); ?>">
      <button type="button" class="btn btn-info">Print</i></button></a>
                               
                    <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>ARTT ID</th>
                                        <th>Student Name</th>
                                        <th>Total Fee</th>
                                        <th>Remaining Fee</th>
                                        <th>Paying Amount</th>
                                        <th>Voucher Date</th>

                                        <th>Due Date</th>
                                        <th>Paid With</th>
                                        <th>Cheque Number</th>


                                     
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($show_voucher_reports_bko as $show) { 

                                            ?>
 <tr> <th><?php  echo $no++;?></th>
                                         <th><?php  echo $show->artt_id; ?></th>
                                        <th><?php  echo $show->fname;    ?></th>
                                        <th><?php  echo $show->total_fees;    ?></th>
                                        <th><?php  echo $show->total_fees-$show->pay_amount;    ?></th>
                                        <th><?php  echo $show->pay_amount;    ?></th>
                                        <th><?php  echo $show->current_date;    ?></th>
                                        <th><?php  echo $show->due_date;    ?></th>
                                        <th><?php  echo $show->pay_with;    ?></th>
                                        <th><?php  echo $show->cheque_number;    ?></th>
                                       
                                     
                                    </tr>

                                    <?php
                                }

                                    ?>



                           
                                    </tbody>
                                </table>
                
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>















