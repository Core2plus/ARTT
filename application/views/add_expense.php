<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Expense
        <small></small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">ADD DETAIL</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body table-responsive ">
                    <?php $this->load->helper("form"); ?>               
                                <br>                
                                <form action="<?php echo base_url('insert_expense'); ?>" method="post" enctype="multipart/form-data">
                                   

                                    
                                        
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Title</label>
                                            <div class="col-md-4">
                                                <input type="text" name="expense_title" class="form-control" required placeholder="Enter Title">
                                            </div>

                                            <label class="col-md-2">Sub Title</label>
                                            <div class="col-md-4">
                                                <input type="text" name="expense_sub_title" class="form-control" placeholder="Enter Sub Title" required placeholder="Surname">
                                            </div>
                                        </div>
                                         <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                             <label class="col-md-2">Current Date</label>
                                            <div class="col-md-4">
                                           <input type="date" name="today_date" style="border:none;" value="<?php echo date('Y-m-d'); ?>" />
                                            </div>


                                            <label class="col-md-2">Due Date</label>
                                            <div class="col-md-4">
                                                <input type="date" name="due_date" class="form-control" required >
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Amount</label>
                                            <div class="col-md-4">
                                                <input type="number" placeholder="Enter Amount" name="amount" class="form-control" required>
                                            </div>

                                        </div>


                                        <div class="form-group row col-md-10 col-sm-10 col-xs-12">
                                            <label class="col-md-2">Expense Details</label>
                                            <div class="col-md-7">
                                                <textarea class="form-control" name = "expense_details" rows="2" id="comment" placeholder="Enter Details"></textarea>
                                            </div>

                                           
                                           
                                        </div>
                                        

                                    </div>
                                    
                                    
                                    
                                    
                                    <!-- <div class="form-group">
                                        <label>Select Subject<span class="required">*</span></label>
                                        <div>
                                            
                                            <select class="form-control" id="sel1">
                                          <option class="form-control">Enable</option>
                                          <option class="form-control">Disable</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <div>

<button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                           
                                        </div>
                                    </div>


                                </form>

                                </div>
                                </div>
                               
                
            
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        
    </section>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>