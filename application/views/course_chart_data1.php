<?php print_r($rmyarray);?>

<script>
window.onload = function () {

var chart = new CanvasJS.Chart("chartContainer", {
  exportEnabled: true,
  animationEnabled: true,
  title:{
    text: "Fee Collection Analytics"
  },
   
  axisX: {
    title: "Courses"
  },
  axisY: {
    title: "Courses Actual Amount",
    titleFontColor: "#4F81BC",
    lineColor: "#4F81BC",
    labelFontColor: "#4F81BC",
    tickColor: "#4F81BC"
  },
  

  toolTip: {
    shared: true,
  },
  legend: {
    cursor: "pointer",
    itemclick: toggleDataSeries
  },
  data: [{
    type: "column",
    name: "Actual Fee",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Fee",
    indexLabel: "{y}",
         
    dataPoints: <?php echo @$tmyarray;?>
  },
  {
    type: "column",
    name: "Receive Fee",
    axisYType: "secondary",
    showInLegend: true,
    yValueFormatString: "#,##0.# Fee",
    
    dataPoints:<?php echo @$rmyarray;?> 
  },
  {
    type: "column",
    name: "Freeze Amount",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Amount",
    
         
    dataPoints: <?php echo @$b1myarray;?>
  },
  {
    type: "column",
    name: "Balance Amount",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Amount",
    
         
    dataPoints: <?php echo @$b2myarray;?>
  },
  {
    type: "column",
    name: "Discount",
    showInLegend: true,      
    yValueFormatString: "#,##0.# Amount",
   
         
    dataPoints: <?php echo @$dmyarray;?>
  },


  ]
});
chart.render();

function toggleDataSeries(e) {
  if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
    e.dataSeries.visible = false;
  } else {
    e.dataSeries.visible = true;
  }
  e.chart.render();
}

}
</script>

<div class="content-wrapper bgimage" >
<section class="content-header">
      <h1 style="color:#000;"><i class="fa fa-users"></i> Fee Collection Analytics</h1>
    </section>
    <!-- Main content -->
    <section class="content" >

      <div class="row">
       
        <!-- /.col -->
        <div class="col-md-12" >
          <div id="chartContainer" style="height: 300px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
              <!-- /.nav-tabs-custom -->
              <div class="box-body table-responsive">




                     <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Code Name</th>
                                        <th>Program</th>
                                        <th>Total Student</th>
                                        <th>Total Fee</th>
                                        <th>Total Received</th>
                                        <th>Freeze</th>
                                        <th>Discount</th>
                                        <th>Balance Amount</th>
                                        
                                       
                        
                        
                    </tr>
                </thead>
                <tbody>
                 <?php
                  $no = 1;
                   foreach ($courses as $course) {
                     
                   ?>
               
                <tr>
                   <td><?php echo $no++; ?></td>
                   <td><?php echo $course['coursecode']; ?></td>
                                        <td><?php echo $course['coursename']; ?></td> 
                                        <td><?php echo $course['Total_Student']; ?></td>
                                         <td><?php echo $course['Total_CourseFee']; ?></td>
                                        <td><?php echo $course['TotRcvFeeAmt']; ?></td>
                                        <td><?php  echo $course['Frz_Rcv_Amt']; ?></td>
                                        <td><?php  echo $course['Discount_Amt']; ?></td>
                                        <td><?php  echo $course['Bal_2']; ?></td>
                                        
                        
                  
                </tr>
              
 
               <?php } ?>
                </tbody>
                
              </table>











                  
                    
                
                </div><!-- /.box-body -->
        </div>
      </div>
        <!-- /.col -->
      
      <!-- /.row -->

    </section>

  </div>

  